/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes;

public class KubernetesClientException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public KubernetesClientException(String url, Throwable t) {
		super("Error using url '" + url + "'", t);
	}
	
	public KubernetesClientException(String url, String message) {
		super("Error using url '" + url + "': " + message);
	}
	
	public KubernetesClientException(String url, String message, Throwable t) {
		super("Error using url '" + url + "': " + message, t);
	}
}
