/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes;

/**
 * Representation of a watch event as used by the Kubernetes websocket API
 * @param <T> The type of resource watched
 */
public class WatchUpdate<T> {
	private final String m_type;
	private final T m_data;

	public WatchUpdate(String type, T data) {
		m_type = type;
		m_data = data;
	}

	public String getType() {
		return m_type;
	}

	public T getData() {
		return m_data;
	}
}
